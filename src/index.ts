interface MarkBoxResult{
    success:Boolean
    message:String
}

interface ValidateWinResult{
    win:Boolean
    result:Number[][]
}

enum DireccionDiagonal { LeftToRigth = 1, RigthToLeft= -1}
enum TypeLine { Row = 1, Column= 2}

class DashboardTicTacToe {
    
    _boxes: Boolean[][]

    constructor(_boxes: Boolean[][] = []){
        if(_boxes && _boxes.length ){
            if(_boxes.length != _boxes[0].length)
                throw Error("Estructura del TIC TAC TOE no es valida.")
            this._boxes = _boxes
        }
        else{
            this._boxes = [[false, false, false] , [false, false, false], [false, false, false] ]
        }
    }

    public get boxes() : Boolean[][] {
        return this._boxes
    }

    public set boxes(_boxes:Boolean[][]){
        this._boxes = _boxes
    }
    
    markBox(y:number, x:number): MarkBoxResult{
        if(this._boxes[y][x]){
            return {
                success: false,
                message: "Casilla ya marcada."
            } as MarkBoxResult
        }
        else{
            this._boxes[y][x] = true
            return {
                success: true,
                message: "Casilla ya marcada correctamente."
            } as MarkBoxResult
        }
    }

    isMarkBox(y:number, x:number): Boolean{
        return this._boxes[y][x]
    }

    validateWin(){
        console.log(this.validateDiagonal(DireccionDiagonal.LeftToRigth))
        console.log(this.validateDiagonal(DireccionDiagonal.RigthToLeft))
        console.log(this.validateLines(TypeLine.Row))
        console.log(this.validateLines(TypeLine.Column))
    }

    private validateDiagonal(direction:number){
        let long_x:number = direction == 1? 0 : this._boxes[0].length - 1
        let long_y:number = 0
        let plays:Number[][] = []
        let index:number = 0
        let win:Boolean = true
        while((this._boxes.length > 0 && this._boxes[0].length > 0) && this._boxes.length > index){
            plays[index] = [long_y,long_x]
            if(!this._boxes[long_y][long_x]){
                win = false
                break
            }
            long_x = long_x + direction
            long_y = long_y + 1
            index++
        }

        return {
            win,
            result: win? plays:[]
        } as ValidateWinResult
    }

    private validateLines(type_line:number){
        let win:Boolean = true
        let plays:Number[][] = []
        if(type_line == TypeLine.Row){
            let index_y:number = 0
            while(index_y < this._boxes.length){
                let index_x:number = 0
                win = true
                plays = []
                while(index_x < this._boxes[0].length){
                    plays[index_x] = [index_y, index_x]
                    if(!this._boxes[index_y][index_x]){
                        win = false
                        break
                    }
                    index_x++
                }

                if(win){
                    break
                }
                index_y++
            }
        }
        else{
            let index_x:number = 0
            while(index_x < this._boxes[0].length){
                let index_y:number = 0
                win = true
                plays = []
                while(index_y < this._boxes.length){
                    plays[index_y] = [index_y, index_x]
                    if(!this._boxes[index_y][index_x]){
                        win = false
                        break
                    }
                    index_y++
                }

                if(win){
                    break
                }
                index_x++
            }
        }
        return {
            win,
            result: win? plays:[]
        } as ValidateWinResult
    }

}

class Game{
    players:Player[]

    constructor(){
        this.players = []
    }

    playPlayer(player: Player, y:number, x:number): void{
        this.players.forEach((_player)=>{
            if(_player.id == player.id){
                if(_player.dashboardTicTacToe.isMarkBox(y, x)){

                }
            }
        })
    }


}

class Player{

    private _dashboardTicTacToe:DashboardTicTacToe
    private _id:Number

    constructor(_id:Number){
        this._dashboardTicTacToe = new DashboardTicTacToe()
        this._id = _id
    }

    public get dashboardTicTacToe(): DashboardTicTacToe {
        return this._dashboardTicTacToe
    }

    public set dashboardTicTacToe(_dashboardTicTacToe:DashboardTicTacToe) {
        this._dashboardTicTacToe = _dashboardTicTacToe
    }

    public get id(): Number {
        return this._id
    }

    public set id(_id:Number) {
        this._id = _id
    }
}

const tic_tac_toe= new DashboardTicTacToe()

console.log(tic_tac_toe.markBox(1,1))
console.log(tic_tac_toe.markBox(1,1))
console.log(tic_tac_toe.validateWin())
